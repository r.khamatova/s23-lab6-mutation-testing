from bonus_system import calculateBonuses

def assertCalculationIsCorrect(program, amount, expectedResult):
    assert calculateBonuses(program, amount) == expectedResult


def testStandard():
    assertCalculationIsCorrect('Standard', 9999, 1*0.5)
    assertCalculationIsCorrect('Standard', 10000, 1.5*0.5)
    assertCalculationIsCorrect('Standard', 49999, 1.5*0.5)
    assertCalculationIsCorrect('Standard', 50000, 2*0.5)
    assertCalculationIsCorrect('Standard', 99999, 2*0.5)
    assertCalculationIsCorrect('Standard', 100000, 2.5*0.5)
    assertCalculationIsCorrect('Standard', 199999, 2.5*0.5)


def testPremium():
    assertCalculationIsCorrect('Premium', 5000, 1*0.1)
    
    
def testDiamond():
    assertCalculationIsCorrect('Diamond', 5000, 1*0.2)


def testOther():
    assertCalculationIsCorrect('Other', 5000, 1*0)
    assertCalculationIsCorrect('Standarc', 5000, 1*0)
    assertCalculationIsCorrect('Standare', 5000, 1*0)
    assertCalculationIsCorrect('Premiul', 5000, 1*0)
    assertCalculationIsCorrect('Premiun', 5000, 1*0)
    assertCalculationIsCorrect('Diamonc', 5000, 1*0)
    assertCalculationIsCorrect('Diamone', 5000, 1*0)
